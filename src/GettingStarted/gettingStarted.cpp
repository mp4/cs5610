#include <sb7.h>
#include <vmath.h>

#include <object.h>
#include <sb7ktx.h>
#include <shader.h>
#include <assert.h>
#include <map>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>
#include <cfloat>
#include "../CustomOBJLoader.h"
#define STB_IMAGE_IMPLEMENTATION
#include "../stb_image.h"

#define PI 3.14159265

class gettingStarted_app : public sb7::application
{

protected:
    void init()
    {
        static const char title[] = "Final Project Signed Distance Fields";

        sb7::application::init();

        memcpy(info.title, title, sizeof(title));
		info.windowWidth = 1024;
		info.windowHeight = 1024;
    }

    void startup();
    void render(double currentTime);
	void sceneInternalRender(vmath::mat4 perspective_matrix, vmath::mat4 view_matrix);
	void sceneInternalRenderProjection(vmath::mat4 perspective_matrix, vmath::mat4 view_matrix, vmath::vec4 plane);
    void onKey(int key, int action);
	void onMouseMove(int x, int y);
	void onMouseButton(int button, int action);
	void genSphereData();
	vmath::vec3 getArcballVector(int x, int y);

	//just uniforms here'

    struct uniforms_block
    {
        vmath::mat4     view_matrix;
		vmath::mat4     model_matrix;
        vmath::mat4     proj_matrix;
		vmath::mat4	    shadowProj_matrix;
		vmath::vec4		emmission;
		vmath::vec4		cameraPosition;
		vmath::vec4     shadowParams;
		vmath::vec3		incolor;
		float vcolorpct;
		vmath::vec4     lightPos;
		vmath::vec4 lightIntensity;
		vmath::vec4	ambient;
		vmath::vec4 diffuse;
		vmath::vec4 specular;
		float shininess;
		float reflectivity;
    };
  
	// Variables for mouse interaction
    bool bPerVertex;
	bool bShiftPressed = false;
	bool bZoom = false;
	bool bRotate = false;
	bool bPan = false;

	int iWidth = info.windowWidth;
	int iHeight = info.windowHeight;

	

	// Rotation and Translation matricies for moving the camera by mouse interaction.
	vmath::mat4 rotationMatrix = vmath::mat4::identity();
	vmath::mat4 translationMatrix = vmath::mat4::identity();

private:
	// Variables for mouse position to solve the arcball vectors
	int iPrevMouseX = 0;
	int iPrevMouseY = 0;
	int iCurMouseX = 0;
	int iCurMouseY = 0;

	// Scale of the objects in the scene
	float fScale = 7.0f;

	// Initial position of the camera
	float fXpos = 0.0f;
	float fYpos = 0.0f;
	float fZpos = 75.0f;
	
	int viewModes = 7;
	float sprReflectivity = 0.5f;

	float sphereMixpct = 1.0f;
	bool sphereVcolor = true;
	bool toonShading = false;

	vmath::vec4 lightPos = { 0, 0, 0, 1 };
	vmath::vec3 cbsprTrans = { 0, 0, 0 };

	vmath::vec4 shadowParameters = {0.5f, 10000.0f, 0.005f,0.0f};

	GLuint vaocubeN;
	const static int NUM_CUBE_N_BUFS = 4;
	GLuint cubenbufs[NUM_CUBE_N_BUFS];

	GLuint vaoteapot;
	const static int NUM_TEAPOT_BUFS = 4;
	GLuint teapotbufs[NUM_TEAPOT_BUFS];
	int trisInteapot;

	GLuint vaosphere;
	const static int NUM_SPHERE_BUFS = 4;
	GLuint spherebufs[NUM_SPHERE_BUFS];
	int verticesInSphere;

	GLuint vaoplane;
	const static int NUM_PLANE_BUFS = 4;
	GLuint planebufs[NUM_PLANE_BUFS];


	GLuint planeTex0;
	GLuint helloDistanceFieldTex;
	GLuint lasVegasSDFTex;
	GLuint neonSDFTex;
	GLuint scaledBaseTex;

	GLuint cubenProgram;
	GLuint sdfnoaliasProgram;
	GLuint sdfoutlineProgram;
	GLuint baseImageProgram;
	GLuint neonLightingProgram;
	GLuint dropShadowsProgram;

	const unsigned int MAX_MODES = 7;
	unsigned int currentMode = 0;

	void loadModels();
	void loadTextures();
	void loadShaders();
	void otherInitialization();
};


static GLuint loadtexstb(std::string fname, int comp)
{
	GLuint tex;
	int width, height, components;
	glGenTextures(1, &tex);

	glBindTexture(GL_TEXTURE_2D, tex);

	float * data = (float *)stbi_load(fname.c_str(),&width, &height, &components,comp);
	assert(data);
	assert(width > 0);
	assert(height > 0);
	assert(components > 0 && components < 5);
	switch (components)
	{
	case STBI_grey:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
		break;
	case STBI_grey_alpha:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, width, height, 0, GL_RG, GL_UNSIGNED_BYTE, data);
		break;
	case STBI_rgb:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		break;
	case STBI_rgb_alpha:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		break;
	}
	

	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(data);
	return tex;
}
//note groundplane is the plane normal and lightpos is in worl space
//based on algorithm from http://gamedev.stackexchange.com/questions/27252/draw-a-projection-of-a-mesh-on-a-surface
static vmath::mat4 shadowMatrix(vmath::vec4 groundplane ,vmath::vec4 lightpos)
{
	vmath::mat4 shadowMat;
	float dot = vmath::dot(groundplane, lightpos);

	shadowMat[0][0] = dot - lightpos[0] * groundplane[0];
    shadowMat[1][0] = 0.f - lightpos[0] * groundplane[1];
    shadowMat[2][0] = 0.f - lightpos[0] * groundplane[2];
    shadowMat[3][0] = 0.f - lightpos[0] * groundplane[3];

    shadowMat[0][1] = 0.f - lightpos[1] * groundplane[0];
    shadowMat[1][1] = dot - lightpos[1] * groundplane[1];
    shadowMat[2][1] = 0.f - lightpos[1] * groundplane[2];
    shadowMat[3][1] = 0.f - lightpos[1] * groundplane[3];

    shadowMat[0][2] = 0.f - lightpos[2] * groundplane[0];
    shadowMat[1][2] = 0.f - lightpos[2] * groundplane[1];
    shadowMat[2][2] = dot - lightpos[2] * groundplane[2];
    shadowMat[3][2] = 0.f - lightpos[2] * groundplane[3];

    shadowMat[0][3] = 0.f - lightpos[3] * groundplane[0];
    shadowMat[1][3] = 0.f - lightpos[3] * groundplane[1];
    shadowMat[2][3] = 0.f - lightpos[3] * groundplane[2];
    shadowMat[3][3] = dot - lightpos[3] * groundplane[3];


	return shadowMat;
}

void gettingStarted_app::loadModels()
{
#pragma region
	{
		glGenVertexArrays(1, &vaocubeN);
		glBindVertexArray(vaocubeN);

		glGenBuffers(NUM_CUBE_N_BUFS, cubenbufs);
		Loader cubel;
		cubel.load("cubewN.obj");

		auto plv = cubel.getVertices();
		glBindBuffer(GL_ARRAY_BUFFER, cubenbufs[0]);
		glBufferData(GL_ARRAY_BUFFER,

			plv.second*sizeof(float),
			plv.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		auto cbn = cubel.getNormals(3);
		glBindBuffer(GL_ARRAY_BUFFER, cubenbufs[2]);
		glBufferData(GL_ARRAY_BUFFER,

			cbn.second*sizeof(float),
			cbn.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		auto cbt = cubel.getColors(3);
		glBindBuffer(GL_ARRAY_BUFFER, cubenbufs[1]);
		glBufferData(GL_ARRAY_BUFFER,

			cbt.second*sizeof(float),
			cbt.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);

		//bind the uniforms
		glBindBuffer(GL_UNIFORM_BUFFER, cubenbufs[3]);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(uniforms_block), NULL, GL_STATIC_DRAW);
		glBindVertexArray(0);
	}
#pragma endregion
	
#pragma region
	{
		glGenVertexArrays(1, &vaoteapot);
		glBindVertexArray(vaoteapot);

		glGenBuffers(NUM_TEAPOT_BUFS, teapotbufs);
		Loader cubel;
		cubel.load("sphereWN.obj");

		auto plv = cubel.getVertices();
		glBindBuffer(GL_ARRAY_BUFFER, teapotbufs[0]);
		glBufferData(GL_ARRAY_BUFFER,

			plv.second*sizeof(float),
			plv.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		trisInteapot = plv.second / 12;

		auto cbn = cubel.getNormals(3);
		glBindBuffer(GL_ARRAY_BUFFER, teapotbufs[2]);
		glBufferData(GL_ARRAY_BUFFER,

			cbn.second*sizeof(float),
			cbn.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		auto cbt = cubel.getColors(3);
		glBindBuffer(GL_ARRAY_BUFFER, teapotbufs[1]);
		glBufferData(GL_ARRAY_BUFFER,

			cbt.second*sizeof(float),
			cbt.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);


		//bind the uniforms
		glBindBuffer(GL_UNIFORM_BUFFER, teapotbufs[3]);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(uniforms_block), NULL, GL_STATIC_DRAW);
		glBindVertexArray(0);
	}
#pragma endregion
	
#pragma region
	{
		glGenVertexArrays(1, &vaosphere);
		glBindVertexArray(vaosphere);

		glGenBuffers(NUM_SPHERE_BUFS, spherebufs);
		Loader cubel;
		cubel.load("sphereWN.obj");

		auto plv = cubel.getVertices();
		glBindBuffer(GL_ARRAY_BUFFER, spherebufs[0]);
		glBufferData(GL_ARRAY_BUFFER,

			plv.second*sizeof(float),
			plv.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		verticesInSphere = plv.second/4;

		auto cbn = cubel.getNormals(3);
		glBindBuffer(GL_ARRAY_BUFFER, spherebufs[2]);
		glBufferData(GL_ARRAY_BUFFER,

			cbn.second*sizeof(float),
			cbn.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		auto cbt = cubel.getColors(3);
		glBindBuffer(GL_ARRAY_BUFFER, spherebufs[1]);
		glBufferData(GL_ARRAY_BUFFER,

			cbt.second*sizeof(float),
			cbt.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);

		//bind the uniforms
		glBindBuffer(GL_UNIFORM_BUFFER, spherebufs[3]);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(uniforms_block), NULL, GL_STATIC_DRAW);
		glBindVertexArray(0);
	}
#pragma endregion

#pragma region
	{
		glGenVertexArrays(1, &vaoplane);
		glBindVertexArray(vaoplane);

		glGenBuffers(NUM_PLANE_BUFS, planebufs);
		Loader cubel;
		cubel.load("plane.obj");

		auto plv = cubel.getVertices();
		glBindBuffer(GL_ARRAY_BUFFER, planebufs[0]);
		glBufferData(GL_ARRAY_BUFFER,

			plv.second*sizeof(float),
			plv.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		auto cbn = cubel.getNormals(3);
		glBindBuffer(GL_ARRAY_BUFFER, planebufs[2]);
		glBufferData(GL_ARRAY_BUFFER,

			cbn.second*sizeof(float),
			cbn.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		auto cbt = cubel.getColors(3);
		glBindBuffer(GL_ARRAY_BUFFER, planebufs[1]);
		glBufferData(GL_ARRAY_BUFFER,

			cbt.second*sizeof(float),
			cbt.first,
			GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);

		//bind the uniforms
		glBindBuffer(GL_UNIFORM_BUFFER, planebufs[3]);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(uniforms_block), NULL, GL_STATIC_DRAW);
		glBindVertexArray(0);
	}
#pragma endregion
	glBindVertexArray(0);
}


void gettingStarted_app::loadTextures()
{
	//myImageTex = loadtexstb("IMG_0211.jpg", 3);
	//glGenerateTextureMipmap(myImageTex);

	helloDistanceFieldTex = loadtexstb("hello4.png", 1);
	glGenerateTextureMipmap(helloDistanceFieldTex);


	scaledBaseTex = loadtexstb("helloShrunk.png", 1);
	glGenerateTextureMipmap(scaledBaseTex);

	lasVegasSDFTex = loadtexstb("Lasvegas4.png", 1);
	glGenerateTextureMipmap(lasVegasSDFTex);

	neonSDFTex = loadtexstb("neonLighting4.png", 1);
	glGenerateTextureMipmap(neonSDFTex);

	glBindTexture(GL_TEXTURE_2D, 0);
}



void gettingStarted_app::loadShaders()
{
#pragma region
	{
		cubenProgram = glCreateProgram();
		GLuint vs = sb7::shader::load("cuben.glsl-vs", GL_VERTEX_SHADER);
		GLuint fs = sb7::shader::load("cuben.glsl-fs", GL_FRAGMENT_SHADER);
		GLint success = 0;
		glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			const int DEBUG_SIZE = 1024;
			char debug[DEBUG_SIZE];

			glGetShaderInfoLog(fs, DEBUG_SIZE, 0, debug);
			OutputDebugString(debug);
			std::cout << debug << std::endl;

		}
		assert(success != GL_FALSE);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
		assert(success != GL_FALSE);

		glAttachShader(cubenProgram, vs);
		glAttachShader(cubenProgram, fs);

		//glBindAttribLocation(color_per_pix_program, 0, "position");
		//glBindAttribLocation(color_per_pix_program, 1, "vcolor");



		glLinkProgram(cubenProgram);
		success = 0;
		glGetProgramiv(cubenProgram, GL_LINK_STATUS, &success);
		assert(success != GL_FALSE);

		glUseProgram(cubenProgram);
		glUniform1i(glGetUniformLocation(cubenProgram, "tex"), 6);
		glUniform1i(glGetUniformLocation(cubenProgram, "distanceField"), 7);
	}
#pragma endregion

#pragma region
	{
	    sdfnoaliasProgram = glCreateProgram();
		GLuint vs = sb7::shader::load("cuben.glsl-vs", GL_VERTEX_SHADER);
		GLuint fs = sb7::shader::load("sdfnoalias.glsl-fs", GL_FRAGMENT_SHADER);
		GLint success = 0;
		glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			const int DEBUG_SIZE = 1024;
			char debug[DEBUG_SIZE];

			glGetShaderInfoLog(fs, DEBUG_SIZE, 0, debug);
			OutputDebugString(debug);
			std::cout << debug << std::endl;

		}
		assert(success != GL_FALSE);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
		assert(success != GL_FALSE);

		glAttachShader(sdfnoaliasProgram, vs);
		glAttachShader(sdfnoaliasProgram, fs);

		//glBindAttribLocation(color_per_pix_program, 0, "position");
		//glBindAttribLocation(color_per_pix_program, 1, "vcolor");



		glLinkProgram(sdfnoaliasProgram);
		success = 0;
		glGetProgramiv(sdfnoaliasProgram, GL_LINK_STATUS, &success);
		assert(success != GL_FALSE);

		glUseProgram(sdfnoaliasProgram);
		glUniform1i(glGetUniformLocation(sdfnoaliasProgram, "tex"), 6);
		glUniform1i(glGetUniformLocation(sdfnoaliasProgram, "distanceField"), 7);
	}
#pragma endregion

#pragma region
	{
		baseImageProgram = glCreateProgram();
		GLuint vs = sb7::shader::load("cuben.glsl-vs", GL_VERTEX_SHADER);
		GLuint fs = sb7::shader::load("baseImage.glsl-fs", GL_FRAGMENT_SHADER);
		GLint success = 0;
		glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			const int DEBUG_SIZE = 1024;
			char debug[DEBUG_SIZE];

			glGetShaderInfoLog(fs, DEBUG_SIZE, 0, debug);
			OutputDebugString(debug);
			std::cout << debug << std::endl;

		}
		assert(success != GL_FALSE);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
		assert(success != GL_FALSE);

		glAttachShader(baseImageProgram, vs);
		glAttachShader(baseImageProgram, fs);

		//glBindAttribLocation(color_per_pix_program, 0, "position");
		//glBindAttribLocation(color_per_pix_program, 1, "vcolor");



		glLinkProgram(baseImageProgram);
		success = 0;
		glGetProgramiv(baseImageProgram, GL_LINK_STATUS, &success);
		assert(success != GL_FALSE);

		glUseProgram(baseImageProgram);
		glUniform1i(glGetUniformLocation(baseImageProgram, "tex"), 6);
		glUniform1i(glGetUniformLocation(baseImageProgram, "distanceField"), 7);
	}
#pragma endregion

#pragma region
	{
		neonLightingProgram = glCreateProgram();
		GLuint vs = sb7::shader::load("cuben.glsl-vs", GL_VERTEX_SHADER);
		GLuint fs = sb7::shader::load("neonLighting.glsl-fs", GL_FRAGMENT_SHADER);
		GLint success = 0;
		glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			const int DEBUG_SIZE = 1024;
			char debug[DEBUG_SIZE];

			glGetShaderInfoLog(fs, DEBUG_SIZE, 0, debug);
			OutputDebugString(debug);
			std::cout << debug << std::endl;

		}
		assert(success != GL_FALSE);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
		assert(success != GL_FALSE);

		glAttachShader(neonLightingProgram, vs);
		glAttachShader(neonLightingProgram, fs);

		//glBindAttribLocation(color_per_pix_program, 0, "position");
		//glBindAttribLocation(color_per_pix_program, 1, "vcolor");



		glLinkProgram(neonLightingProgram);
		success = 0;
		glGetProgramiv(neonLightingProgram, GL_LINK_STATUS, &success);
		assert(success != GL_FALSE);

		glUseProgram(neonLightingProgram);
		glUniform1i(glGetUniformLocation(neonLightingProgram, "tex"), 6);
		glUniform1i(glGetUniformLocation(neonLightingProgram, "distanceField"), 7);
	}
#pragma endregion

#pragma region
	{
		sdfoutlineProgram = glCreateProgram();
		GLuint vs = sb7::shader::load("cuben.glsl-vs", GL_VERTEX_SHADER);
		GLuint fs = sb7::shader::load("sdfoutline.glsl-fs", GL_FRAGMENT_SHADER);
		GLint success = 0;
		glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			const int DEBUG_SIZE = 1024;
			char debug[DEBUG_SIZE];

			glGetShaderInfoLog(fs, DEBUG_SIZE, 0, debug);
			OutputDebugString(debug);
			std::cout << debug << std::endl;

		}
		assert(success != GL_FALSE);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
		assert(success != GL_FALSE);

		glAttachShader(sdfoutlineProgram, vs);
		glAttachShader(sdfoutlineProgram, fs);

		//glBindAttribLocation(color_per_pix_program, 0, "position");
		//glBindAttribLocation(color_per_pix_program, 1, "vcolor");



		glLinkProgram(sdfoutlineProgram);
		success = 0;
		glGetProgramiv(sdfoutlineProgram, GL_LINK_STATUS, &success);
		assert(success != GL_FALSE);

		glUseProgram(sdfoutlineProgram);
		glUniform1i(glGetUniformLocation(sdfoutlineProgram, "tex"), 6);
		glUniform1i(glGetUniformLocation(sdfoutlineProgram, "distanceField"), 7);
	}
#pragma endregion


#pragma region
	{
		dropShadowsProgram = glCreateProgram();
		GLuint vs = sb7::shader::load("cuben.glsl-vs", GL_VERTEX_SHADER);
		GLuint fs = sb7::shader::load("dropShadow.glsl-fs", GL_FRAGMENT_SHADER);
		GLint success = 0;
		glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			const int DEBUG_SIZE = 1024;
			char debug[DEBUG_SIZE];

			glGetShaderInfoLog(fs, DEBUG_SIZE, 0, debug);
			OutputDebugString(debug);
			std::cout << debug << std::endl;

		}
		assert(success != GL_FALSE);
		glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
		assert(success != GL_FALSE);

		glAttachShader(dropShadowsProgram, vs);
		glAttachShader(dropShadowsProgram, fs);

		//glBindAttribLocation(color_per_pix_program, 0, "position");
		//glBindAttribLocation(color_per_pix_program, 1, "vcolor");



		glLinkProgram(dropShadowsProgram);
		success = 0;
		glGetProgramiv(dropShadowsProgram, GL_LINK_STATUS, &success);
		assert(success != GL_FALSE);

		glUseProgram(dropShadowsProgram);
		glUniform1i(glGetUniformLocation(sdfoutlineProgram, "tex"), 6);
		glUniform1i(glGetUniformLocation(sdfoutlineProgram, "distanceField"), 7);
	}
#pragma endregion
	glUseProgram(0);//make sure there is no program set when this completes
}

void gettingStarted_app::otherInitialization()
{
	glEnable(GL_PROGRAM_POINT_SIZE);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LEQUAL);
}

void gettingStarted_app::startup()
{
	otherInitialization();
	loadShaders();
	loadTextures();
	loadModels();
}


void gettingStarted_app::render(double currentTime)
{
//initialiation of matrices and buffer setup
#pragma region
    static const GLfloat zeros[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    static const GLfloat gray[] = { 0.1f, 0.1f, 0.1f, 0.0f };
	static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
	static const GLfloat skyBlue[] = { 0.529f, 0.808f, 0.922f };
    static const GLfloat ones[] = { 1.0f };
    const float f = (float)currentTime;
	
	// Calculations for mouse interaction camera rotation and translation matrix
	float fAngle = 0.0f;
	vmath::vec3 axis_in_camera_coord = (0.0f, 1.0f, 0.0f);	
	if (iCurMouseX != iPrevMouseX || iCurMouseY != iPrevMouseY) {
		// Arcball Rotation
		if (bRotate){
			vmath::vec3 va = getArcballVector(iPrevMouseX, iPrevMouseY);
			vmath::vec3 vb = getArcballVector(iCurMouseX, iCurMouseY);
			fAngle = acos(fmin(1.0f, vmath::dot(va, vb)));
			axis_in_camera_coord = vmath::cross(va, vb);
			axis_in_camera_coord = vmath::normalize(axis_in_camera_coord);
			iPrevMouseX = iCurMouseX;
			iPrevMouseY = iCurMouseY;
			rotationMatrix *= vmath::rotate(vmath::degrees(fAngle), axis_in_camera_coord);
		}
		// Zoom in and out
		if (bZoom) {
			fZpos += (iCurMouseY - iPrevMouseY);
			if (fZpos > 500)
			{
				fZpos = 500;
			}
			else if (fZpos < 10)
			{
				fZpos = 10;
			}
			iPrevMouseY = iCurMouseY;
			iPrevMouseX = iCurMouseX;
		}
		// Pan camera left, right, up, and down
		if (bPan) {
			fXpos += (iCurMouseX - iPrevMouseX);
			fYpos += (iCurMouseY - iPrevMouseY);
			iPrevMouseY = iCurMouseY;
			iPrevMouseX = iCurMouseX;
			translationMatrix = vmath::translate(fXpos / (info.windowWidth / fZpos), -fYpos/(info.windowWidth / fZpos), 0.0f);
		}
	}

	// Set up view and perspective matrix
	vmath::vec3 view_position = vmath::vec3(0.0f, 0.0f, fZpos);
	vmath::mat4 view_matrix = vmath::lookat(view_position,
		vmath::vec3(0.0f, 0.0f, 0.0f),
		vmath::vec3(0.0f, 1.0f, 0.0f));
	view_matrix *= translationMatrix;
	view_matrix *= rotationMatrix;

    glViewport(0, 0, info.windowWidth, info.windowHeight);

	// Create sky blue background
    glClearBufferfv(GL_COLOR, 0, skyBlue);
    glClearBufferfv(GL_DEPTH, 0, ones);
	vmath::mat4 perspective_matrix = vmath::perspective(90.0f, (float)info.windowWidth / (float)info.windowHeight, 0.5f, 10000.0f);
	switch (currentMode)
	{
	case 0:
		glUseProgram(cubenProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, helloDistanceFieldTex);
		break;
	case 1:
		glUseProgram(sdfnoaliasProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, helloDistanceFieldTex);
		break;
	case 2:
		glUseProgram(baseImageProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, scaledBaseTex);
		break;
	case 3:
		glUseProgram(cubenProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, lasVegasSDFTex);
		break;
	case 4:
		glUseProgram(neonLightingProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, neonSDFTex);
		break;
	case 5:
		glUseProgram(sdfoutlineProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, neonSDFTex);
		break;
	case 6:
		glUseProgram(dropShadowsProgram);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, planeTex0);
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, helloDistanceFieldTex);
		break;
	default:
		break;
	}
	
	//vmath::vec3 view_position = vmath::vec3(0.0f, 0.0f, fZpos);
	{
		//draw the plane

		glBindVertexArray(vaoplane);
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, planebufs[3]);
		uniforms_block * block = (uniforms_block *)glMapBufferRange(GL_UNIFORM_BUFFER, 0, sizeof(uniforms_block), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

		vmath::mat4 model_matrix =
			//vmath::rotate((float)currentTime * 14.5f, 0.0f, 1.0f, 0.0f) *
			vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f)*
			vmath::scale(fScale);

		//block->mv_matrix = view_matrix * vmath::translate(vmath::vec3(-42.0f, -7.0f, 0.0f))  *model_matrix;// *vmath::scale(vmath::vec3(7, 7, 7));

		block->view_matrix = view_matrix;
		block->model_matrix = vmath::translate(cbsprTrans) * model_matrix * vmath::scale(10.0f, 10.0f, 10.0f);
		block->proj_matrix = perspective_matrix;
		block->shadowParams = shadowParameters;
		block->shadowParams[2] = 0.00001f;
		block->shadowParams[3] = 1.0f;
		block->emmission = vmath::vec4((float)currentTime);
		block->incolor = vmath::vec3(1, 0, 0);
		block->vcolorpct = sphereMixpct;//change to a variable to be changed on input
		block->lightPos = lightPos;
		block->lightIntensity = vmath::vec4(1, 1, 1, 1);
		block->ambient = vmath::vec4(0.1f, 0.1f, 1.0f, 1);
		block->diffuse = vmath::vec4(0.5, 0.5, 0, 1);
		block->specular = vmath::vec4(0.01f, 0.01f, 0, 0);
		block->shininess = 2.0f;
		block->reflectivity = 0.0f;

		glUnmapBuffer(GL_UNIFORM_BUFFER);
		glDrawArrays(GL_TRIANGLES, 0, 6);

	}
}

void gettingStarted_app::onKey(int key, int action)
{
	// Check to see if shift was pressed
	if (action == GLFW_PRESS && (key == GLFW_KEY_LEFT_SHIFT || key == GLFW_KEY_RIGHT_SHIFT))
	{
		bShiftPressed = true;
	}
	if (action)
	{
		static bool currentTexMode = false;
		static bool currentShader = true;
		const float REF_STEP = 0.05f;
		const float TRANS_STEP = 0.5f;
        switch (key)
        {
			
            case 'R': 
				rotationMatrix = vmath::mat4::identity();
				translationMatrix = vmath::mat4::identity();
				fXpos = 0.0f;
				fYpos = 0.0f;
				fZpos = 75.0f;
				lightPos = vmath::vec4(0,0,0,1);
                break;
			case 'C':
				//switch colors
				sphereVcolor = !sphereVcolor;
				if (sphereVcolor)
				{
					sphereMixpct = 1.0f;
				}
				else
				{
					sphereMixpct = 0.0f;
				}
				break;
			case 'V':
				//switch shaders

				break;
			case 'M':
				

				break;
			case '[':
				currentMode--;
				if (currentMode > MAX_MODES)
				{
					currentMode = MAX_MODES -1;
				}
				break;
			case ']':
				currentMode++;
				currentMode = currentMode % MAX_MODES;
			case 'F':
				toonShading = !toonShading;
				break;
			case 'W':
				lightPos[0] += TRANS_STEP;
				break;
			case 'S':
				lightPos[0] -= TRANS_STEP;
				break;
			case 'A':
				lightPos[1] -= TRANS_STEP;
				break;
			case 'D':
				lightPos[1] += TRANS_STEP;
				break;
			case 'Q':
				lightPos[2] -= TRANS_STEP;
				break;
			case 'E':
				lightPos[2] += TRANS_STEP;
				break;
		}
    }
	// Check to see if shift was released
	if (action == GLFW_RELEASE) {
		bShiftPressed = false;
	}
}


void gettingStarted_app::onMouseButton(int button, int action)
{
	int x, y;
	
	getMousePosition(x, y);
	// Check to see if left mouse button was pressed for rotation
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		bRotate = true;
		iPrevMouseX = iCurMouseX = x;
		iPrevMouseY = iCurMouseY = y;
	}
	// Check to see if right mouse button was pressed for zoom and pan
	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
		bZoom = false;
		bPan = false;
		if (bShiftPressed == true)
		{
			bZoom = true;
		}
		else if (bShiftPressed == false)
		{
			bPan = true;
		}
		iPrevMouseX = iCurMouseX = x;
		iPrevMouseY = iCurMouseY = y;
	}
	else {
		bRotate = false;
		bZoom = false;
		bPan = false;
	}
	
}

void gettingStarted_app::onMouseMove(int x, int y)
{
	// If rotating, zooming, or panning save mouse x and y
	if (bRotate || bZoom || bPan) 
	{
		iCurMouseX = x;
		iCurMouseY = y;
	}
}

// Modified from tutorial at the following website:
// http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Arcball

vmath::vec3 gettingStarted_app::getArcballVector(int x, int y) {
	// find vector from origin to point on sphere
	vmath::vec3 vecP = vmath::vec3(1.0f*x / info.windowWidth * 2 - 1.0f, 1.0f*y / info.windowHeight * 2 - 1.0f, 0.0f);
	// inverse y due to difference in origin location on the screen
	vecP[1] = -vecP[1];
	float vecPsquared = vecP[0] * vecP[0] + vecP[1] * vecP[1];
	// solve for vector z component
	if (vecPsquared <= 1)
		vecP[2] = sqrt(1-vecPsquared);		
	else
		vecP = vmath::normalize(vecP);
	return vecP;
}



DECLARE_MAIN(gettingStarted_app)
