#ifndef MP4_OBJ_LOADER
#define MP4_OBJ_LOADER

#include <string>
#include <iostream>
#include <fstream>
#include <regex>
#include <vector>
#include <WinBase.h>
#include <utility>

struct Vertex
{
	float x, y, z, w;
};
struct Face
{
	Vertex t, l, r;
	Vertex tn, ln, rn;
	Vertex tt, lt, rt;
};


class Loader
{
public:
	Loader(){}
	~Loader();
	void load(std::string path);
	std::pair<float *, int> getNormals(int vecsize = 3, bool ccw = true);
	std::pair<float *, int> getColors(int vecsize = 3, bool ccw = true);
	std::pair<float *, int> getVertices(int vecsize = 4, bool ccw = true);
private:
	float * vertices_data =0;
	float * colors_data =0;
	float * normals_data =0;
	bool ccw = true;
	std::vector<Face> faces;
	std::vector<Vertex> vertices;
	std::vector<Vertex> normals;
	std::vector<Vertex> textures;
};


inline void Loader::load(std::string path)
{
	//only tested / deisgned to work with blender produced files
	std::ifstream ifs(path, std::ifstream::in);
	std::string line;
	const bool debug = false;
	while (ifs.good())
	{
		std::getline(ifs, line);
		/*
		if (debug)
		{
			OutputDebugString(line.c_str());
		}
		*/
		std::regex lineexpr("\\s");
		std::sregex_token_iterator iter(line.begin(), line.end(), lineexpr, -1);

		std::sregex_token_iterator end;
		std::vector<std::string> tokens;
		for (; iter != end; iter++)
		{
			tokens.push_back(*iter);
			/*
			if (debug)
			{
				OutputDebugString(tokens.back().c_str() );

			}
			*/
		}
		if (tokens.size())
		{
			if (tokens[0] == "v")
			{
				if (tokens.size() == 4)
				{
					Vertex v = {stof(tokens[1]), stof(tokens[2]), stof(tokens[3]), 1.0f};
					vertices.push_back(v);
				}
				else if (tokens.size() == 5)
				{
					Vertex v = { stof(tokens[1]), stof(tokens[2]), stof(tokens[3]), stof(tokens[4]) };
					vertices.push_back(v);
				}
				else
				{
					assert(0);//fail if something else happens
				}
			}
			else if (tokens[0] == "vn")
			{
				if (tokens.size() == 4)
				{
					Vertex v = { stof(tokens[1]), stof(tokens[2]), stof(tokens[3]), 1.0f };
					normals.push_back(v);
				}
				else if (tokens.size() == 5)
				{
					Vertex v = { stof(tokens[1]), stof(tokens[2]), stof(tokens[3]), stof(tokens[4]) };
					normals.push_back(v);
				}
				else
				{
					assert(0);//fail if something else happens
				}
			}
			else if (tokens[0] == "vt")
			{
				if (tokens.size() == 3)
				{
					Vertex v = { stof(tokens[1]), stof(tokens[2]), 0.0f, 0.0f };
					textures.push_back(v);
				}
				else if (tokens.size() == 4)
				{
					Vertex v = { stof(tokens[1]), stof(tokens[2]), stof(tokens[3]), 1.0f };
					textures.push_back(v);
				}
				else if (tokens.size() == 5)
				{
					Vertex v = { stof(tokens[1]), stof(tokens[2]), stof(tokens[3]), stof(tokens[4]) };
					textures.push_back(v);
				}
				else
				{
					assert(0);//fail if something else happens
				}
			}
			else if (tokens[0] == "f")
			{
				if (tokens.size() == 4)
				{
					std::regex fexpr("[/\\\\]");
					std::sregex_token_iterator iter(tokens[1].begin(), tokens[1].end(), fexpr, -1);

					std::sregex_token_iterator end;
					std::vector<std::string> token1, token2, token3;
					for (; iter != end; iter++)
					{
						token1.push_back(*iter);
						/*
						if (debug)
						{
							OutputDebugString(token1.back().c_str());

						}
						*/
					}
					iter = std::sregex_token_iterator(tokens[2].begin(), tokens[2].end(), fexpr, -1);
					for (; iter != end; iter++)
					{
						token2.push_back(*iter);
						/*
						if (debug)
						{
							OutputDebugString(token2.back().c_str());

						}
						*/
					}
					iter = std::sregex_token_iterator(tokens[3].begin(), tokens[3].end(), fexpr, -1);
					for (; iter != end; iter++)
					{
						token3.push_back(*iter);
						/*
						if (debug)
						{
							OutputDebugString(token3.back().c_str());

						}
						*/
					}
					if (token1.size() == 3)
					{
						//stoi wil parse the first int found this is okay but if there is more data this will need to be split on /
						Face face = { vertices[stoi(token1[0]) - 1], vertices[stoi(token2[0]) - 1], vertices[stoi(token3[0]) - 1],
							normals[stoi(token1[2]) -1], normals[stoi(token2[2])-1], normals[stoi(token3[2])-1] ,
							textures[stoi(token1[1]) - 1], textures[stoi(token2[1]) - 1], textures[stoi(token3[1]) - 1]
						};
						faces.push_back(face);
					}
					else if (token1.size() == 1)//fill all data with vertex data
					{
						//stoi wil parse the first int found this is okay but if there is more data this will need to be split on /
						Face face = { vertices[stoi(token1[0]) - 1], vertices[stoi(token2[0]) - 1], vertices[stoi(token3[0]) - 1],
							vertices[stoi(token1[0])-1], vertices[stoi(token2[0])-1], vertices[stoi(token3[0])-1] ,
							vertices[stoi(token1[0]) - 1], vertices[stoi(token2[0]) - 1], vertices[stoi(token3[0]) - 1],
						};
						faces.push_back(face);
					}
					else
					{
						assert(0);
					}
					
				}
				else
				{
					assert(0);
				}
			}
			//ignore everthing else
		}

	}



	ifs.close();
}


inline Loader::~Loader()
{
	//cleanup the arrays
	if (vertices_data)
	{
		delete[] vertices_data;
	}
	if (colors_data)
	{
		delete[] colors_data;
	}
	if (normals_data)
	{
		delete[] normals_data;
	}
}
inline std::pair<float *, int> Loader::getNormals(int vecsize, bool ccw)
{
	//currently just return the vertices this will work for the sphere
	if (normals_data)
		delete[] normals_data;
	int array_size = faces.size() * 3 * vecsize;
	normals_data = new float[array_size];
	float * current = normals_data;
	for (int i = 0; i < faces.size(); i++)
	{
		if (ccw)
		{
			//top
			if (vecsize >= 1)
				current[0] = faces[i].tn.x;
			if (vecsize >= 2)
				current[1] = faces[i].tn.y;
			if (vecsize >= 3)
				current[2] = faces[i].tn.z;
			if (vecsize >= 4)
				current[3] = faces[i].tn.w;
			current = current + vecsize;
			//right
			if (vecsize >= 1)
				current[0] = faces[i].ln.x;
			if (vecsize >= 2)
				current[1] = faces[i].ln.y;
			if (vecsize >= 3)
				current[2] = faces[i].ln.z;
			if (vecsize >= 4)
				current[3] = faces[i].ln.w;
			current = current + vecsize;
			//left
			if (vecsize >= 1)
				current[0] = faces[i].rn.x;
			if (vecsize >= 2)
				current[1] = faces[i].rn.y;
			if (vecsize >= 3)
				current[2] = faces[i].rn.z;
			if (vecsize >= 4)
				current[3] = faces[i].rn.w;
			current = current + vecsize;
		}
		else
		{
			//top
			if (vecsize >= 1)
				current[0] = faces[i].tn.x;
			if (vecsize >= 2)
				current[1] = faces[i].tn.y;
			if (vecsize >= 3)
				current[2] = faces[i].tn.z;
			if (vecsize >= 4)
				current[3] = faces[i].tn.w;
			current = current + vecsize;
			//right
			if (vecsize >= 1)
				current[0] = faces[i].rn.x;
			if (vecsize >= 2)
				current[1] = faces[i].rn.y;
			if (vecsize >= 3)
				current[2] = faces[i].rn.z;
			if (vecsize >= 4)
				current[3] = faces[i].rn.w;
			current = current + vecsize;
			//left
			if (vecsize >= 1)
				current[0] = faces[i].ln.x;
			if (vecsize >= 2)
				current[1] = faces[i].ln.y;
			if (vecsize >= 3)
				current[2] = faces[i].ln.z;
			if (vecsize >= 4)
				current[3] = faces[i].ln.w;
			current = current + vecsize;
		}
	}
	return std::pair<float *, int>(normals_data, array_size);
}
inline std::pair<float *, int> Loader::getColors(int vecsize, bool ccw)
{
	if (colors_data)
		delete[] colors_data;
	int array_size = faces.size() * 3 * vecsize;
	colors_data = new float[array_size];
	float * current = colors_data;
	for (int i = 0; i < faces.size(); i++)
	{
		if (ccw)
		{
			//top
			if (vecsize >= 1)
				current[0] = faces[i].tt.x;
			if (vecsize >= 2)
				current[1] = faces[i].tt.y;
			if (vecsize >= 3)
				current[2] = faces[i].tt.z;
			if (vecsize >= 4)
				current[3] = faces[i].tt.w;
			current = current + vecsize;
			//right
			if (vecsize >= 1)
				current[0] = faces[i].lt.x;
			if (vecsize >= 2)
				current[1] = faces[i].lt.y;
			if (vecsize >= 3)
				current[2] = faces[i].lt.z;
			if (vecsize >= 4)
				current[3] = faces[i].lt.w;
			current = current + vecsize;
			//left
			if (vecsize >= 1)
				current[0] = faces[i].rt.x;
			if (vecsize >= 2)
				current[1] = faces[i].rt.y;
			if (vecsize >= 3)
				current[2] = faces[i].rt.z;
			if (vecsize >= 4)
				current[3] = faces[i].rt.w;
			current = current + vecsize;
		}
		else
		{
			//top
			if (vecsize >= 1)
				current[0] = faces[i].tt.x;
			if (vecsize >= 2)
				current[1] = faces[i].tt.y;
			if (vecsize >= 3)
				current[2] = faces[i].tt.z;
			if (vecsize >= 4)
				current[3] = faces[i].tt.w;
			current = current + vecsize;
			//right
			if (vecsize >= 1)
				current[0] = faces[i].rt.x;
			if (vecsize >= 2)
				current[1] = faces[i].rt.y;
			if (vecsize >= 3)
				current[2] = faces[i].rt.z;
			if (vecsize >= 4)
				current[3] = faces[i].rt.w;
			current = current + vecsize;
			//left
			if (vecsize >= 1)
				current[0] = faces[i].lt.x;
			if (vecsize >= 2)
				current[1] = faces[i].lt.y;
			if (vecsize >= 3)
				current[2] = faces[i].lt.z;
			if (vecsize >= 4)
				current[3] = faces[i].lt.w;
			current = current + vecsize;
		}
	}
	return std::pair<float *, int>(colors_data, array_size);
}
inline std::pair<float *, int> Loader::getVertices(int vecsize, bool ccw)
{
	if (vertices_data)
		delete[] vertices_data;
	int array_size = faces.size() * 3 * vecsize;
	vertices_data = new float[array_size];
	float * current = vertices_data;
	for (int i = 0; i < faces.size(); i++)
	{
		if (ccw)
		{
			//top
			if (vecsize >= 1)
				current[0] = faces[i].t.x;
			if (vecsize >= 2)
				current[1] = faces[i].t.y;
			if (vecsize >= 3)
				current[2] = faces[i].t.z;
			if (vecsize >= 4)
				current[3] = faces[i].t.w;
			current = current + vecsize;
			//right
			if (vecsize >= 1)
				current[0] = faces[i].l.x;
			if (vecsize >= 2)
				current[1] = faces[i].l.y;
			if (vecsize >= 3)
				current[2] = faces[i].l.z;
			if (vecsize >= 4)
				current[3] = faces[i].l.w;
			current = current + vecsize;
			//left
			if (vecsize >= 1)
				current[0] = faces[i].r.x;
			if (vecsize >= 2)
				current[1] = faces[i].r.y;
			if (vecsize >= 3)
				current[2] = faces[i].r.z;
			if (vecsize >= 4)
				current[3] = faces[i].r.w;
			current = current + vecsize;
		}
		else
		{
			//top
			if (vecsize >= 1)
				current[0] = faces[i].t.x;
			if (vecsize >= 2)
				current[1] = faces[i].t.y;
			if (vecsize >= 3)
				current[2] = faces[i].t.z;
			if (vecsize >= 4)
				current[3] = faces[i].t.w;
			current = current + vecsize;
			//right
			if (vecsize >= 1)
				current[0] = faces[i].r.x;
			if (vecsize >= 2)
				current[1] = faces[i].r.y;
			if (vecsize >= 3)
				current[2] = faces[i].r.z;
			if (vecsize >= 4)
				current[3] = faces[i].r.w;
			current = current + vecsize;
			//left
			if (vecsize >= 1)
				current[0] = faces[i].l.x;
			if (vecsize >= 2)
				current[1] = faces[i].l.y;
			if (vecsize >= 3)
				current[2] = faces[i].l.z;
			if (vecsize >= 4)
				current[3] = faces[i].l.w;
			current = current + vecsize;
		}
	}
	return std::pair<float *, int>(vertices_data, array_size);
}
//returns an array of vec4
inline std::pair<float *, int> generateTangentsCube(float * vertices, float * normals, float * textures)
{
	const int arraySize = 36 * 4;
	float * tangents = new float[arraySize];

	int current = 0;

	for (int i = 0; i < 36; i++)
	{
		if (normals[current + 0] > 0.1f)
		{
			//-z tangent + y bitangent
		}
		else if (normals[current + 0] < -0.1f)
		{
			//+z -y bitangent
		}
		else if (normals[current + 1] > 0.1f)
		{
			//+x, +z
		}
		else if (normals[current + 1] < -0.1f)
		{
			//-x, -z
		}
		else if (normals[current + 2] > 0.1f)
		{
		}
		else if (normals[current + 2] < -0.1f)
		{
		}
	}

	return std::pair<float *, int>(tangents, arraySize);
}

#endif